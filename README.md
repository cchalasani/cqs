cqs
=====

A call queue service for prototype of gen_statem

Context
-----

# Queue
A queue is a servicable entity for calls or chat with automatic distribution to a collection of agents

# Agent
A one to one servicing entity for calls or chat that is delegated by a queue

## Agent States
### Presence States
 - offline
 - online
 - break
 - dnd
### Session States
 - session
 - wrapup
 - supervision
 - handover
 - hold

