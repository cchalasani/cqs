-module(cqs_agent_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
    SupFlags = #{strategy => rest_for_one,
                 intensity => 3,
                 period => 60},
    ChildSpecs = [#{id => cqs_agent_sofo, start => {cqs_agent_sofo, start_link, []}, type =>
                        supervisor},
                  #{id => cqs_agent_man, start => {cqs_agent_man, start_link, []}}],
    {ok, {SupFlags, ChildSpecs}}.

