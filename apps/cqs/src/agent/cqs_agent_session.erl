-module(cqs_agent_session).
-behaviour(gen_statem).

-define(WRAPUP_TIMEOUT, 30000).
-define(MAX_SESSION_TIME, 3600000).

%% gen_statem callbacks

-export([init/1,
         callback_mode/0]).

%% gen_statem state functions

-export([session/3,
         supervision/3,
         wrapup/3]).

%%% gen_statem callbacks

init(Args) -> erlang:error(not_implemented, [Args]).

callback_mode() -> [state_functions, state_enter].

%%% gen_statem state functions

session(enter, _, _) ->
    {keep_state_and_data, {{timeout, max_session_time}, ?MAX_SESSION_TIME, end_session}};
session(cast, end_session, {Agent, Session}) ->
    {next_state, wrapup, {Agent, Session}, {{timeout, max_session_time}, cancel}};
session({call, From}, supervise, {Agent, Session}) ->
    {next_state, supervision, {Agent, Session}, {reply, From, ok}};
session({timeout, max_session_time}, end_session, {Agent, Session}) ->
    {next_state, wrapup, {Agent, Session}};
session({call, From}, state, _) ->
    {keep_state_and_data, {reply, From, session}};
session(_, _, _) ->
    {keep_state_and_data, postpone}.

supervision(enter, _, _) ->
    keep_state_and_data;
supervision(cast, end_supervise, {Agent, Session}) ->
    {next_state, session, {Agent, Session}};
supervision({timeout, max_session_time}, end_session, {Agent, Session}) ->
    {next_state, session, {Agent, Session}, postpone};
supervision({call, From}, state, _) ->
    {keep_state_and_data, {reply, From, supervision}};
supervision(cast, end_session, {Agent, Session}) ->
    {next_state, session, {Agent, Session}, postpone};
supervision(_, _, _) ->
    {keep_state_and_data, postpone}.

wrapup(enter, _, _) ->
    {keep_state_and_data, {state_timeout, ?WRAPUP_TIMEOUT, end_wrapup}};
wrapup(state_timeout, end_wrapup, {Agent, _}) ->
    {next_state, online, Agent, pop_callback_module};
wrapup({call, From}, state, _) ->
    {keep_state_and_data, {reply, From, wrapup}};
wrapup(_, _, _) ->
    {keep_state_and_data, postpone}.
