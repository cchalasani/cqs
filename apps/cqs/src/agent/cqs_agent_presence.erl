-module(cqs_agent_presence).
-behaviour(gen_statem).

%% gen_statem callbacks

-export([init/1,
         callback_mode/0,
         handle_event/4]).

%%% gen_statem callbacks

init([Agent]) ->
    {ok, offline, Agent}.

callback_mode() -> handle_event_function.

handle_event({call, From}, {start_session, Session}, online, Agent) ->
    {next_state, session, {Agent, Session}, [{reply, From, ok}, {push_callback_module, cqs_agent_session}]};
handle_event({call, From}, {start_session, _}, State, _) ->
    {keep_state_and_data, {reply, From, {error, State}}};
handle_event({call, From}, state, State, _) -> {keep_state_and_data, {reply, From, State}};
handle_event(cast, OldState, OldState, _) -> keep_state_and_data;
handle_event(cast, NewState, _, Agent) -> {next_state, NewState, Agent};
handle_event(EventType, EventData, State, StateData) ->
    io:format("Unexpected event ~p~n", [{EventType, EventData, State, StateData}]).
