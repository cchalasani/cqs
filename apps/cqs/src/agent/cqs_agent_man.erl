-module(cqs_agent_man).

-behaviour(gen_server).

-define(AGENTS, [tom, jerry, donald, mickey, archie, goofy]).

%% API

-export([start_link/0,
         start_agents/0,
         stop_agents/0,
         all_agents/0,
         online_agents/0,
         online_agents/1]).

%% gen_server callbacks

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%% API

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

start_agents() ->
    gen_server:cast(?MODULE, start_agents).

stop_agents() ->
    gen_server:cast(?MODULE, stop_agents).

all_agents() ->
    gen_server:call(?MODULE, all_agents).

online_agents() ->
    gen_server:call(?MODULE, online_agents).

online_agents(Agents) ->
    gen_server:call(?MODULE, {online_agents, Agents}).

%%% gen_server callbacks

init([]) ->
    {ok, ?AGENTS, 0}.

handle_call(all_agents, _From, Agents) ->
    {reply, Agents, Agents};
handle_call(online_agents, _From, Agents) ->
    Reply = [ Agent || Agent <- Agents, cqs_agent:state(Agent) =:= online ],
    {reply, Reply, Agents};
handle_call({online_agents, AgentsRequested}, _From, Agents) ->
    Reply = [ Agent || Agent <- AgentsRequested, lists:member(Agent, Agents), cqs_agent:state(Agent) =:= online ],
    {reply, Reply, Agents};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

handle_cast(start_agents, Agents) ->
    do_start_agents(Agents),
    {noreply, Agents};
handle_cast(stop_agents, Agents) ->
    do_stop_agents(Agents),
    {noreply, Agents};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(timeout, Agents) ->
    do_start_agents(Agents),
    {noreply, Agents};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Internal functions

do_start_agents(Agents) ->
    lists:foreach(
      fun(Agent) ->
              cqs_agent_sofo:start_child(Agent)
      end,
      Agents
     ).

do_stop_agents(Agents) ->
    lists:foreach(
      fun(Agent) ->
              cqs_agent_sofo:stop_child(Agent)
      end,
      Agents
     ).
