-module(cqs_agent_sofo).

-behaviour(supervisor).

%% API
-export([start_link/0,
         start_child/1,
         delete_child/1]).

%% Supervisor callbacks
-export([init/1]).

%%% API functions

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_child(Agent) ->
    case whereis(Agent) of
        undefined ->
            supervisor:start_child(?MODULE, [Agent]);
        _ ->
            ok
    end.

delete_child(Agent) ->
    case whereis(Agent) of
        undefined -> ok;
        Pid ->
            supervisor:terminate_child(?MODULE, Pid)
    end.

%%% Supervisor callbacks

init([]) ->
    SupFlags = #{strategy => simple_one_for_one
                ,intensity => 3
                ,period => 60},
    ChildSpec = #{id => cqs_agent, start => {cqs_agent, start_link, []}
                 ,restart => transient},
    {ok, {SupFlags, [ChildSpec]}}.

