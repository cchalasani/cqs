-module(cqs_agent).

%% API

-export([start_link/1,
         state/1,
         online/1,
         offline/1,
         pause/1,
         start_session/2,
         end_session/1,
         supervise/1,
         end_supervise/1]).

%%% API

start_link(Agent) ->
    gen_statem:start_link({local, Agent}, cqs_agent_presence, [Agent], []).

state(Agent) ->
    gen_statem:call(Agent, state).

online(Agent) ->
    gen_statem:cast(Agent, online).

offline(Agent) ->
    gen_statem:cast(Agent, offline).

pause(Agent) ->
    gen_statem:cast(Agent, pause).

start_session(Agent, Session) ->
    gen_statem:call(Agent, {start_session, Session}).

end_session(Agent) ->
    gen_statem:cast(Agent, end_session).

supervise(Agent) ->
    gen_statem:call(Agent, supervise).

end_supervise(Agent) ->
    gen_statem:cast(Agent, end_supervise).
