-module(cqs_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->
    SupFlags = #{strategy => one_for_all,
                 intensity => 3,
                 period => 60},
    ChildSpecs = [#{id => cqs_queue_sup, start => {cqs_queue_sup, start_link, []}, type =>
                        supervisor},
                  #{id => cqs_agent_sup, start => {cqs_agent_sup, start_link, []}, type =>
                        supervisor}],
    {ok, {SupFlags, ChildSpecs}}.

