-module(cqs_queue_man).

-behaviour(gen_server).

-define(QUEUES, [#{name => support, agents => [tom, jerry, donald]},
                 #{name => purchase, agents => [mickey, archie, goofy]},
                 #{name => membership, agents => [tom, donald, mickey]},
                 #{name => grievance, agents => [archie, jerry, goofy]}]).

%% API

-export([start_link/0,
         start_queues/0,
         stop_queues/0,
         all_queues/0,
         all_sessions/0]).

%% gen_server callbacks

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%% API

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

start_queues() ->
    gen_server:cast(?MODULE, start_queues).

stop_queues() ->
    gen_server:cast(?MODULE, stop_queues).

all_queues() ->
    gen_server:call(?MODULE, all_queues).

all_sessions() ->
    gen_server:call(?MODULE, all_sessions).

%%% gen_server callbacks

init([]) ->
    {ok, ?QUEUES, 0}.

handle_call(all_queues, _From, Queues) ->
    {reply, Queues, Queues};
handle_call(all_sessions, _From, Queues) ->
    Reply = do_get_all_sessions(Queues),
    {reply, Reply, Queues};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

handle_cast(start_queues, Queues) ->
    do_start_queues(Queues),
    {noreply, Queues};
handle_cast(stop_queues, Queues) ->
    do_stop_queues(Queues),
    {noreply, Queues};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(timeout, Queues) ->
    do_start_queues(Queues),
    {noreply, Queues};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Internal functions

do_start_queues(Queues) ->
    lists:foreach(
      fun(Queue) ->
              cqs_queue_sofo:start_child(Queue)
      end,
      Queues
     ).

do_stop_queues(Queues) ->
    lists:foreach(
      fun(Queue) ->
              cqs_queue_sofo:stop_child(Queue)
      end,
      Queues
     ).

do_get_all_sessions(Queues) ->
    lists:foldl(
      fun(#{name := Name}, AllSessions) ->
              Sessions = cqs_queue:sessions(Name),
              if map_size(Sessions) =:= 0 -> AllSessions;
                 true -> AllSessions#{Name => Sessions} end
      end,
      #{},
      Queues
     ).
