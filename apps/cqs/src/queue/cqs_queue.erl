-module(cqs_queue).

-behaviour(gen_server).

%% API

-export([start_link/1,
         start_session/1,
         end_session/2,
         sessions/1]).

%% gen_server callbacks

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%% API

start_link(#{name := Name} = Config) ->
    gen_server:start_link({local, Name}, ?MODULE, [Config], []).

start_session(Name) ->
    gen_server:call(Name, start_session).

end_session(Name, SessionId) ->
    gen_server:cast(Name, {end_session, SessionId}).

sessions(Name) ->
    gen_server:call(Name, sessions).

%%% gen_server callbacks

init([Config]) ->
    {ok, Config#{sessions => #{}}}.

handle_call(start_session, _From, State) ->
    {Reply, NewState} = do_start_session(State),
    {reply, Reply, NewState};
handle_call(sessions, _From, #{sessions := Sessions} = State) ->
    {reply, Sessions, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

handle_cast({end_session, SessionId}, State) ->
    NewState = do_end_session(SessionId, State),
    {noreply, NewState};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Internal functions

do_start_session(#{agents := Agents, sessions := Sessions} = State) ->
    case cqs_agent_man:online_agents(Agents) of
        [] -> {{error, no_online_agent}, State};
        [Agent|_] ->
            SessionId = session_id(),
            ok = cqs_agent:start_session(Agent, SessionId),
            {{ok, SessionId}, State#{sessions => Sessions#{SessionId => Agent}}}
    end.

do_end_session(SessionId, #{sessions := Sessions} = State) ->
    case maps:take(SessionId, Sessions) of
        {Agent, NewSessions} ->
            cqs_agent:end_session(Agent),
            State#{sessions => NewSessions};
        error -> State
    end.

session_id() -> erlang:integer_to_list(erlang:system_time(), 36).

