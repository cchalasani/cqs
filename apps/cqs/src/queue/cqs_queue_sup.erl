-module(cqs_queue_sup).

-behaviour(supervisor).

%% API

-export([start_link/0]).

%% Supervisor callbacks

-export([init/1]).

-define(SERVER, ?MODULE).

%%% API

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%% Supervisor callbacks

init([]) ->
    SupFlags = #{strategy => rest_for_one,
                 intensity => 3,
                 period => 60},
    ChildSpecs = [#{id => cqs_queue_sofo, start => {cqs_queue_sofo, start_link, []}, type =>
                        supervisor},
                  #{id => cqs_queue_man, start => {cqs_queue_man, start_link, []}}],
    {ok, {SupFlags, ChildSpecs}}.

