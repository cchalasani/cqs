-module(cqs_queue_sofo).

-behaviour(supervisor).

%% API

-export([start_link/0,
         start_child/1,
         delete_child/1]).

%% Supervisor callbacks

-export([init/1]).

%%% API functions

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_child(#{name := Name} = Queue) ->
    case whereis(Name) of
        undefined ->
            supervisor:start_child(?MODULE, [Queue]);
        _ ->
            ok
    end.

delete_child(#{name := Name}) ->
    case whereis(Name) of
        undefined -> ok;
        Pid ->
            supervisor:terminate_child(?MODULE, Pid)
    end.

%%% Supervisor callbacks

init([]) ->
    SupFlags = #{strategy => simple_one_for_one
                ,intensity => 3
                ,period => 60},
    ChildSpec = #{id => cqs_queue, start => {cqs_queue, start_link, []}
                 ,restart => transient},
    {ok, {SupFlags, [ChildSpec]}}.

